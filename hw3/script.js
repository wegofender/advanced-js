class Game {
    constructor() {
        this.field = document.getElementById('game');
        this.level = null;
        this.timer = null;
        this.userScore = 0;
        this.AIScore = 0;
        this.startBtn = document.getElementById('start-btn');
        this.levelSelect = document.getElementById('level');
        this.startBtn.addEventListener('click', this.settingsHandler.bind(this));
        this.bindedClickHandler = this.userClickHandler.bind(this);
    }

    static get STYLES() {
        return {
            'active': 'active',
            'user': 'user',
            'AI': 'ai'
        }
    };

    static get LEVELS() {
        return {
            easy: 1500,
            medium: 1000,
            hard: 100
        }
    };

    static getRandomHole() {
        const freeHoles = document.querySelectorAll('.mole:not(.ai):not(.user)');
        const randomIndex = Math.floor(Math.random() * freeHoles.length);
        return freeHoles[randomIndex];
    }

    settingsHandler() {
        const chosenLevel = this.levelSelect.value;
        this.level = chosenLevel;
        this.startBtn.setAttribute('disabled', 'true');
        this.levelSelect.setAttribute('disabled', 'true');
        this.startGame()
    }

    startGame() {
        if (!this.isGameFinished()) {
            const currentHole = Game.getRandomHole();

            currentHole.classList.add(Game.STYLES['active']);
            this.timer = setTimeout(this.AIHandler.bind(this, currentHole, this.level), Game.LEVELS[this.level]);
            currentHole.addEventListener('click', this.bindedClickHandler)
        } else {
            this.finishGame()
        }
    }

    userClickHandler(e) {
        this.userScore++;
        e.target.classList.add(Game.STYLES['user']);
        e.target.removeEventListener('click', this.bindedClickHandler);
        clearTimeout(this.timer);
        this.startGame(this.level);
    }

    AIHandler(currentHole, level) {
        currentHole.classList.remove(Game.STYLES['active']);
        currentHole.classList.add(Game.STYLES['AI']);
        currentHole.removeEventListener('click', this.bindedClickHandler);

        this.AIScore++;
        this.startGame(level)
    }

    isGameFinished() {
        return this.AIScore >= 50 || this.userScore >= 50
    }

    finishGame() {
        if (this.AIScore > this.userScore) {
            alert('You Lose!')
        } else if (this.AIScore < this.userScore) {
            alert('You Win!')
        } else {
            alert('Draw!')
        }
        this.startBtn.removeAttribute('disabled');
        [...this.field.querySelectorAll('.mole')].forEach(item => {
            item.classList.remove('ai', 'user', 'active');
        });

        this.levelSelect.removeAttribute('disabled');
        this.startBtn.removeAttribute('disabled');
        this.AIScore = 0;
        this.userScore = 0;
    }
}

const whackAMole = new Game();
